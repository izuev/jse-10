package ru.tsc.izuev.tm.api;

public interface ICommandController {

    void showErrorArgument();

    void showErrorCommand();

    void showVersion();

    void showAbout();

    void showSystemInfo();

    void showHelp();

}
