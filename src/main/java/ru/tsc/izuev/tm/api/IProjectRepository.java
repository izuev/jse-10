package ru.tsc.izuev.tm.api;

import ru.tsc.izuev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void clear();

}
