package ru.tsc.izuev.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
