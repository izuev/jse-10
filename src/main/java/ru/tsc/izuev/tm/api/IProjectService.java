package ru.tsc.izuev.tm.api;

import ru.tsc.izuev.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
