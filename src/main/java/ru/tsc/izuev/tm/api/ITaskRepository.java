package ru.tsc.izuev.tm.api;

import ru.tsc.izuev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void clear();

}
