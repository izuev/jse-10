package ru.tsc.izuev.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
