package ru.tsc.izuev.tm.api;

import ru.tsc.izuev.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
