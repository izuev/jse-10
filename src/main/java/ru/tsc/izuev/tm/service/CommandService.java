package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.ICommandRepository;
import ru.tsc.izuev.tm.api.ICommandService;
import ru.tsc.izuev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
